﻿using OntologyClasses.BaseClasses;
using Priority_Tagging_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priority_Tagging_TestingModule
{
    public partial class Form1 : Form
    {
        private PriorityTaggingManager priorityTaggingManager;
        public Form1()
        {
            InitializeComponent();

            priorityTaggingManager = new PriorityTaggingManager();

            toolStripComboBox1.Items.AddRange(priorityTaggingManager.PriorityList.ToArray());
            toolStripComboBox1.ComboBox.DisplayMember = Priority_Tagging_Module.NotifyChanges.PriorityItem_NamePriorityItem;
            toolStripComboBox1.ComboBox.ValueMember = Priority_Tagging_Module.NotifyChanges.PriorityItem_IdPriorityItem;

            if (priorityTaggingManager.ColorDefinitionForm != null)
            {
                priorityTaggingManager.ColorDefinitionForm.Show();
            }

            var priorityReferences = priorityTaggingManager.GetPriorityReferences();
            dataGridView1.DataSource = priorityReferences;
        }

        private void addToPriorityToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            toolStripComboBox1.Enabled = false;
            toolStripComboBox1.SelectedItem = priorityTaggingManager.ClearItem;
            toolStripComboBox1.Enabled = true;
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolStripComboBox1.Enabled)
            {
                var selectedPriority = (PriorityItem) toolStripComboBox1.SelectedItem;

                var result = priorityTaggingManager.SetPriorityTag(new OntologyClasses.BaseClasses.clsOntologyItem
                {
                    GUID = "d13d350a2a0d421386239efec15ffc85",
                    Name = "18.12.2015 00:51:18: Ich muss sensibel vorgehen. Als ich sie auf ihre Kinder angesprochen habe, brach sie zusammen. Wir mussten das Telefonat beenden.",
                    GUID_Parent = "351d45912495450182aba425f5235db9",
                    Type = priorityTaggingManager.LocalConfig.Globals.Type_Object
                }, selectedPriority);

                
            }
            

        }
    }
}
